/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-blob-image')
    .component('bitcraftDbEditionBlobImage', {
        templateUrl: './js/db-edition-blob-image/db-edition-blob-image.template.html', // this line will be replaced
        bindings: {
            value: '=',
            edit: '<'
        }
    });
